﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace gkama.nlp.finance.data
{
    public class BasicModel : IBasicModel
    {
        [JsonProperty("id")] public string id { get; set; }
        [JsonProperty("name")] public string name { get; set; }
    }
}
