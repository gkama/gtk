﻿namespace gkama.nlp.finance.data
{
    public interface IBasicModel
    {
        string id { get; set; }
        string name { get; set; }
    }
}
