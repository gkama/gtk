﻿using System.Collections.Generic;

namespace gkama.nlp.finance.data
{
    public interface ISubModel
    {
        string id { get; set; }
        string name { get; set; }
        string details { get; set; }
        IEnumerable<SubModel> children { get; set; }
        List<IBasicModel> full_category_family { get; set; }
        bool HasChildren { get; }
    }
}
