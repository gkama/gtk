﻿using Microsoft.Extensions.Configuration;

namespace gkama.nlp.finance.data
{
    public interface IEnvironmentSettings
    {
        IConfiguration Configuration { get; }
        string ConString { get; }
        string NlpDbName { get; }
        string ModelsColName { get; }
        string ModelSettingsColName { get; }
        string NlpFinancialModelId { get; }
    }
}
