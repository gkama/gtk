﻿using System.Collections.Generic;

namespace gkama.nlp.finance.data
{
    public class Category : ICategory
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<string> matched_words { get; set; } = new List<string>();
    }
}
