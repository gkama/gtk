﻿using System.Collections.Generic;

namespace gkama.nlp.finance.data
{
    public interface IModel
    {
        string id { get; set; }
        string tenant_id { get; set; }
        string model_name { get; set; }
        IEnumerable<SubModel> model { get; set; }
    }
}
