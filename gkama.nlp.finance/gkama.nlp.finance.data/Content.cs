﻿using Newtonsoft.Json;

namespace gkama.nlp.finance.data
{
    public class Content : IContent
    {
        [JsonProperty("model_id")] public string model_id { get; set; }
        [JsonProperty("content")] public string value { get; set; }
    }
}
