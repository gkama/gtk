﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gkama.nlp.finance.data
{
    public interface IContent
    {
        string model_id { get; set; }
        string value { get; set; }
    }
}
