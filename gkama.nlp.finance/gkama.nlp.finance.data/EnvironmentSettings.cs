﻿using Microsoft.Extensions.Configuration;

namespace gkama.nlp.finance.data
{
    public class EnvironmentSettings : IEnvironmentSettings
    {
        public IConfiguration Configuration { get; }

        public EnvironmentSettings(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }

        public string ConString => Configuration["constring"];
        public string NlpDbName => Configuration["nlp_dbname"];
        public string ModelsColName => Configuration["nlp_col_model"];
        public string ModelSettingsColName => Configuration["nlp_col_modelsettings"]; 
        public string NlpFinancialModelId => Configuration["nlp_modelid_financial"];
    }
}
