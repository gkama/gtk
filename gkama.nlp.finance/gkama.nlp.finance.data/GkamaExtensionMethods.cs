﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace gkama.nlp.finance.data
{
    public static class GkamaExtensionMethods
    {
        public static bool ContainsCategory(this IEnumerable<ICategory> categories, string categoryId)
        {
            if (categories == null) return false;
            if (!categories.Any()) return false;

            if (categories
                .FirstOrDefault(x => x.id == categoryId) != null)
                return true;

            return false;
        }

        public static List<string> SaveSplitAsList(this string details, char delimiter)
        {
            if (string.IsNullOrWhiteSpace(details)) return new List<string>();

            return new List<string>(details
                .Split(delimiter,
                    StringSplitOptions.RemoveEmptyEntries));
        }

        public static string[] SaveSplitAsArray(this string details, char delimiter)
        {
            if (string.IsNullOrWhiteSpace(details)) return new List<string>().ToArray();

            return new List<string>(details
                .Split(delimiter,
                    StringSplitOptions.RemoveEmptyEntries))
                .ToArray();
        }

        /// <summary>
        /// Formats a <see cref="string"/> source to indented JSON. 
        /// This is done by using <see cref="Newtonsoft.Json.JsonConvert.DeserializeObject(string)"/> to an object and then <see cref="Newtonsoft.Json.JsonConvert.SerializeObject(object)"/> back to an indented <see cref="string"/>
        /// </summary>
        /// <param name="Source"></param>
        /// <returns></returns>
        public static string FormatAsJson(this string source)
        {
            return JsonConvert
                .SerializeObject(JsonConvert
                    .DeserializeObject(source),
                    Formatting.Indented);
        }
    }
}
