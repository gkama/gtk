﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace gkama.nlp.finance.data
{
    public class FinancialModelSettings : IModelSettings
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId _id { get; set; }

        [BsonElement("id")]
        [JsonProperty("id")]
        public string id { get; set; }

        [BsonElement("model_id")]
        [JsonProperty("model_id")]
        public string model_id { get; set; }

        [BsonElement("model_name")]
        [JsonProperty("model_name")]
        public string model_name { get; set; }

        [BsonElement("details_delimiter")]
        [JsonProperty("details_delimiter")]
        public char details_delimiter { get; set; }

        [BsonElement("tokenizer_delimiters")]
        [JsonProperty("tokenizer_delimiters")]
        public char[] tokenizer_delimiters { get; set; }

        [BsonElement("scrub_words")]
        [JsonProperty("scrub_words")]
        public string[] scrub_words { get; set; }
    }
}
