﻿using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace gkama.nlp.finance.data
{
    public class FinancialModel : IModel
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId _id { get; set; }

        [BsonElement("id")]
        [JsonProperty("id")]
        public string id { get; set; }

        [BsonElement("tenant_id")]
        [JsonProperty("tenant_id")]
        public string tenant_id { get; set; }

        [BsonElement("model_name")]
        [JsonProperty("model_name")]
        public string model_name { get; set; }

        [BsonElement("model")]
        [JsonProperty("model")]
        public IEnumerable<SubModel> model { get; set; }


        /*
         * in order to keep a total parent/child tree we need to keep track of a sorted list
         * of hierarcy - first id is the top level model, second id is the second level, etc.
         */
        public void Init()
        {
            var financeModel = this.model
                .FirstOrDefault();

            financeModel
                .full_category_family
                .Add(new BasicModel { id = financeModel.id, name = financeModel.name });

            var stack = new Stack<SubModel>(financeModel.children);
            while (stack.Any())
            {
                var next = stack.Pop();

                next.full_category_family.Insert(0, new BasicModel { id = financeModel.id, name = financeModel.name }); //Always add the top level category at the beginning
                next.full_category_family.Add(new BasicModel { id = next.id, name = next.name }); //Current node Id

                foreach (var child in next.children)
                {
                    child.full_category_family.Add(new BasicModel { id = next.id, name = next.name }); //ParentId
                    stack.Push(child);
                }
            }
        }
    }
}
