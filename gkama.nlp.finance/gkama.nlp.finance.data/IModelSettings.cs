﻿namespace gkama.nlp.finance.data
{
    public interface IModelSettings
    {
        string id { get; set; }
        string model_id { get; set; }
        string model_name { get; set; }
        char details_delimiter { get; set; }
        char[] tokenizer_delimiters { get; set; }
        string[] scrub_words { get; set; }
    }
}
