﻿using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace gkama.nlp.finance.data
{
    public class SubModel : ISubModel
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId _id { get; set; }

        [BsonElement("id")]
        [JsonProperty("id")]
        public string id { get; set; }

        [BsonElement("name")]
        [JsonProperty("name")]
        public string name { get; set; }

        [BsonElement("details")]
        [JsonProperty("details")]
        public string details { get; set; }

        [BsonElement("children")]
        [JsonProperty("children")]
        public IEnumerable<SubModel> children { get; set; }


        [BsonIgnore]
        [JsonIgnore]
        public List<IBasicModel> full_category_family { get; set; } = new List<IBasicModel>();

        [BsonIgnore]
        [JsonIgnore]
        public bool HasChildren
        {
            get
            {
                return this.children.Any();
            }
        }
    }
}
