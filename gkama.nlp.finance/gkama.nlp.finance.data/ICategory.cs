﻿using System.Collections.Generic;

namespace gkama.nlp.finance.data
{
    public interface ICategory
    {
        string id { get; set; }
        string name { get; set; }
        List<string> matched_words { get; set; }
    }
}
