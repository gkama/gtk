﻿using System;
using System.Net;

using Newtonsoft.Json.Linq;

namespace gkama.nlp.finance.data
{
    public class GkamaException : ApplicationException
    {
        public int status_code { get; set; }
        public string content_type { get; set; } = @"text/plain";

        public GkamaException(int StatusCode)
        {
            this.status_code = StatusCode;
        }

        public GkamaException(string Message) : base(Message)
        {
            this.status_code = 500;
        }

        public GkamaException(int StatusCode, string Message) : base(Message)
        {
            this.status_code = StatusCode;
        }

        public GkamaException(HttpStatusCode StatusCode, string Message) : base(Message)
        {
            this.status_code = (int)StatusCode;
        }

        public GkamaException(int StatusCode, Exception Inner) : this(StatusCode, Inner.ToString()) { }
        public GkamaException(HttpStatusCode StatusCode, Exception Inner) : this(StatusCode, Inner.ToString()) { }
        public GkamaException(int StatusCode, JObject ErrorObject) : this(StatusCode, ErrorObject.ToString()) { this.content_type = @"application/problem+json"; }
    }
}
