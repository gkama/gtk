## gkama.nlp.finance
the high level overview is that the project is a natural language processing (nlp) analyzer for financial words and phrases. you pass in text content and the engine analyzes it and returns the categories it finds. this is useful to mine text content to find what the text is talking about, in terms of financial lingo. this project is currently live at `https://api.gkamacharov.com` as part of a bigger set of APIs

more about the back end - it uses mongodb as the database, c#, asp.net core 2.2 api inftrastructure, and it’s fully hosted on microsoft azure app service (linux) as a SaaS piece

an example of a model:
``` json
{
  "id": "c1903478-3265-4e18-a33a-bdb029c48dcc",
  "tenant_id": "803d5184-fe85-429e-b5f8-bbc0c5431ef7",
  "model_name": "Example",
  "model": [
   {
      "id": "66cb957a-e237-4567-b93c-781005c09935",
      "name": "Finance",
      "details": "finance|financially",
      "children": [
        {
          "id": "cbcb1da7-6d74-42b8-9a14-28a0a1e8ef1d",
          "name": "Money",
          "details": "money|moneys|monye",
          "children": [
            {
              "id": "640c21c4-fb1b-464c-b5bf-78e7ff93286b",
              "name": "Money Management",
              "details": "manage money|management money|mgt money|money manage|money management|money mgt",
              "children": []
            }
          ]
        }
      ]
    }
  ]
}
```

there is a sample request at: `https://api.gkamacharov.com/financial/sample/categorize`
``` json
{
  "content": "This is a test content to test the financial model. The idea is to show how categorization works. Let's talk a bit about money. It is very important to have money. Money management is also very important. But wait! The API can also match on Vanguard index funds such as VBLTX! What about retirement you say? Yes! Cheers!",
  "categories": [
    {
      "id": "66cb957a-e237-4567-b93c-781005c09935",
      "name": "Finance",
      "matched_words": [
        "financial"
      ]
    },
    {
      "id": "984ce69d-de79-478b-9223-ff6349514e19",
      "name": "Index Funds/Vanguard",
      "matched_words": [
        "VBLTX"
      ]
    },
    {
      "id": "3a7868df-13c6-4362-8831-2de4699eaef5",
      "name": "Investing",
      "matched_words": [
        "index fund"
      ]
    },
    {
      "id": "efb9c2a0-a864-42ef-9982-44f1d0384f08",
      "name": "Money",
      "matched_words": [
        "money"
      ]
    },
    {
      "id": "c83ec045-6053-4bb2-8e63-306180ba2e9f",
      "name": "Money/Money Management",
      "matched_words": [
        "money manage",
        "money management"
      ]
    },
    {
      "id": "150930f1-974b-4a98-97f0-91409f2a0820",
      "name": "Retirement",
      "matched_words": [
        "retirement"
      ]
    },
    {
      "id": "1140c833-6ee9-4c89-889a-d9cf1834990b",
      "name": "Vanguard",
      "matched_words": [
        "Vanguard"
      ]
    }
  ]
}
```