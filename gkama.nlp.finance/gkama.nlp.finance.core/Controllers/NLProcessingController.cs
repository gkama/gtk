﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using gkama.nlp.finance.data;
using gkama.nlp.finance.services;

namespace gkama.nlp.finance.core.Controllers
{
    [ApiController]
    public class NLProcessingController : ControllerBase
    {
        private readonly INLProcessingRepository repo;
        private readonly IEnvironmentSettings envSettings;

        public NLProcessingController(INLProcessingRepository nLProcessingRepository, IEnvironmentSettings EnvSettings)
        {
            this.repo = nLProcessingRepository;
            this.envSettings = EnvSettings;
        }

        [HttpGet]
        [Route("/model/{id}")]
        public IActionResult GetModel([FromRoute]string id)
        {
            return new JsonResult(repo.GetModel(id));
        }

        [HttpGet]
        [Route("/model/{id}/sort")]
        public IActionResult SortModel([FromRoute]string id)
        {
            return new JsonResult(repo.SortModel(id));
        }

        [HttpGet]
        [Route("/model/{id}/categories")]
        public IActionResult GetCategories([FromRoute]string id)
        {
            return new JsonResult(repo.GetCategories(id));
        }

        [HttpGet]
        [Route("/model/settings/{id}")]
        public IActionResult GetModelSettings([FromRoute]string id)
        {
            return new JsonResult(repo.GetModelSettings(id));
        }

        [HttpGet]
        [Route("/tenant/{id}")]
        public IActionResult GetTenant([FromRoute]string id)
        {
            return new JsonResult(repo.GetTenant(id));
        }

        [HttpPost]
        [Route("/financial/categorize")]
        public IActionResult Categorize([FromBody]Content content)
        {
            return new JsonResult(repo.Categorize(content.model_id, content.value));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("/financial/sample/categorize")]
        public IActionResult CategorizeSample()
        {
            var testContent = "This is a test content to test the financial model. The idea is to show how " +
                "categorization works. Let's talk a bit about money. It is very important to have money. " +
                "Money management is also very important. But wait! The API can also match on Vanguard index " +
                "funds such as VBLTX! What about retirement you say? Yes! Cheers!";

            return new JsonResult(new
            {
                content = testContent,
                categories = repo.Categorize(envSettings.NlpFinancialModelId, testContent)
            });
        }
    }
}