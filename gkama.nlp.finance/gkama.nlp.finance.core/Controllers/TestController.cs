﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace gkama.nlp.finance.core.Controllers
{
    [AllowAnonymous]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly ILogger log;

        public TestController(IConfiguration configuration, ILogger<TestController> log)
        {
            this.configuration = configuration;
            this.log = log;
        }
       
        [Route("api/test")]
        [HttpGet]
        public JsonResult GetTest()
        {
            return new JsonResult(
                new
                {
                    test1 = "test1",
                    indented = true
                });
        }
    }
}
