﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using gkama.nlp.finance.data;
using gkama.nlp.finance.services;

namespace gkama.nlp.finance.core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddLogging();

            services.AddHealthChecks();

            /*
             * Transient objects are always different; a new instance is provided to every controller and every service
             * Scoped objects are the same within a request, but different across different requests
             * Singleton objects are the same for every object and every request
             */
            services.AddSingleton<IEnvironmentSettings, EnvironmentSettings>();

            /*
             * As of right now - these are all singletons, however they'll most likely
             * be changed to scoped once the logic changes
             */
            services.AddSingleton<INLProcessingRepository, NLProcessingRepository>();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(o =>
                {
                    o.SerializerSettings.Formatting = Formatting.Indented;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {

            }

            app.UseGkamaException();
            app.UseHealthChecks("/ping");

            app.UseMvc();
        }
    }
}
