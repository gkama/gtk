﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

using MongoDB.Driver;

using gkama.nlp.finance.data;

namespace gkama.nlp.finance.services
{
    public class NLProcessingRepository : INLProcessingRepository
    {
        private readonly IEnvironmentSettings EnvSettings;
        private readonly ILogger Log;
        private readonly IMemoryCache MemoryCache;
        private readonly IMongoClient MongoClient;

        private const int DefaultCacheTimeSpan = 86400;

        public NLProcessingRepository(IEnvironmentSettings EnvSettings, ILogger<NLProcessingRepository> Log, IMemoryCache MemoryCache)
        {
            this.EnvSettings = EnvSettings;
            this.Log = Log;
            this.MemoryCache = MemoryCache;

            this.MongoClient = new MongoClient(EnvSettings.ConString);
        }
        public NLProcessingRepository() { }

        public IEnumerable<IModel> GetTenant(string TenantId)
        {
            var Tenant = new List<FinancialModel>();

            Tenant = MemoryCache.GetOrCreate(TenantId, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(DefaultCacheTimeSpan);

                return MongoClient.GetDatabase(EnvSettings.NlpDbName)
                    .GetCollection<FinancialModel>(EnvSettings.ModelsColName)
                    .Find(x => x.tenant_id == TenantId)
                    .ToList();
            });

            if (Tenant == null)
                throw new GkamaException(StatusCodes.Status404NotFound,
                    $"tenant={TenantId} not found");

            return Tenant;
        }

        public IModel GetModel(string ModelId)
        {
            var Model = new FinancialModel();

            Model = MemoryCache.GetOrCreate(ModelId, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(DefaultCacheTimeSpan);

                var model = MongoClient.GetDatabase(EnvSettings.NlpDbName)
                    .GetCollection<FinancialModel>(EnvSettings.ModelsColName)
                    .Find(x => x.id == ModelId)
                    .FirstOrDefault();

                model?.Init();

                return model;
            });

            if (Model == null)
                throw new GkamaException(StatusCodes.Status404NotFound,
                    $"model={ModelId} not found");

            return Model;
        }

        public IModelSettings GetModelSettings(string ModelId)
        {
            var ModelSettings = new FinancialModelSettings();

            ModelSettings = MemoryCache.GetOrCreate($"{ModelId}-settings", entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(DefaultCacheTimeSpan);

                return MongoClient.GetDatabase(EnvSettings.NlpDbName)
                    .GetCollection<FinancialModelSettings>(EnvSettings.ModelSettingsColName)
                    .Find(x => x.model_id == ModelId)
                    .FirstOrDefault();
            });

            if (ModelSettings == null)
                throw new GkamaException(StatusCodes.Status404NotFound,
                    $"model settings with modelid={ModelId} not found");

            return ModelSettings;
        }

        public IModel SortModel(string ModelId)
        {
            var Model = this.GetModel(ModelId);
            var ModelSettings = this.GetModelSettings(ModelId);

            return SortModel(Model, ModelSettings);
        }
        public IModel SortModel(IModel Model, IModelSettings ModelSettings)
        {
            foreach (var subModel in Model.model.FirstOrDefault().children)
            {
                var details = subModel.details
                    .Split(ModelSettings.details_delimiter)
                    .ToList();

                details.Sort();

                subModel.details = string.Join(ModelSettings.details_delimiter, details);

                if (subModel.HasChildren)
                {
                    foreach (var subSubModel in subModel.children)
                    {
                        var subdetails = subSubModel.details
                            .Split(ModelSettings.details_delimiter)
                            .ToList();

                        subdetails.Sort();

                        subSubModel.details = string.Join(ModelSettings.details_delimiter, subdetails);
                    }
                }
            }

            return Model;
        }

        public IEnumerable<string> GetCategories(string ModelId)
        {
            return GetCategories(this.GetModel(ModelId).model);
        }
        public IEnumerable<string> GetCategories(IEnumerable<ISubModel> Models, List<string> Categories = null)
        {
            try
            {
                if (Models == null || Models?.Count() == 0) return Enumerable.Empty<string>();
                if (Categories == null) Categories = new List<string>();

                foreach (var model in Models)
                {
                    if (!Categories.Contains(model.name))
                        Categories.Add(model.name);

                    if (model.HasChildren)
                        GetCategories(model.children, Categories);
                }

                Categories.Sort();

                return Categories;
            }
            catch (Exception e)
            {
                Log.LogError("while retrieving the categories error message={errormsg} stack trace={stacktrace}",
                    e.Message,
                    e.StackTrace);

                throw;
            }
        }

        public IEnumerable<ICategory> Categorize(string ModelId, string Content)
        {
            Log.LogInformation("requesting tokenized categorization with modelid={modelid} and content={content}",
                ModelId,
                Content);

            /*
             * 1) get the model
             * 2) compare the details of each category against the content passed
             * 3) construct the object of categories to be returned
             */
            var baseModel = this.GetModel(ModelId);

            var settings = this.GetModelSettings(baseModel.id.ToString());
            var tokenizedContent = this.Tokenize(Content, settings.tokenizer_delimiters, settings.scrub_words);

            var sw = new Stopwatch();
            sw.Start();
            var categories = this.AdvancedBinarySearch(baseModel, Content, tokenizedContent, settings.details_delimiter);
            sw.Stop();

            Log.LogDebug("advanced binary search algorithm found categories={count} in time={time}",
                categories.Count(),
                sw.ElapsedMilliseconds);

            return categories;
        }

        /// <summary>
        /// Uses a depth first approach to parse through all the models and check their details to run them against the content requested by the end-user
        /// </summary>
        public IEnumerable<ICategory> ParseContent(IEnumerable<ISubModel> Models, char Delimiter, string Content, HashSet<string> TokenizedContent)
        {
            var categories = new List<ICategory>();

            var stack = new Stack<ISubModel>(Models);
            while (stack.Any())
            {
                var next = stack.Pop();

                foreach (var d in this.SaveSplit(next.details, Delimiter))
                {
                    if (d.Contains(' '))
                    {
                        if (Content.Contains(d, StringComparison.InvariantCultureIgnoreCase))
                            UpdateCategories(next, d, ref categories);
                    }
                    else
                    {
                        if (TokenizedContent.Contains(d, StringComparer.InvariantCultureIgnoreCase))
                            UpdateCategories(next, d, ref categories);
                    }
                }

                foreach (var child in next.children)
                    stack.Push(child);
            }

            return categories
                .AsEnumerable()
                .OrderBy(x => x.name);
        }

        public void UpdateCategories(ISubModel Child, string Detail, ref List<ICategory> Categories)
        {
            if (Child == null) return;

            if (!Categories.ContainsCategory(Child.id))
            {
                var nCat = new Category()
                {
                    id = Child.id,
                    name = Child.full_category_family.Count() > 2 && Child.full_category_family
                        .Select(x => x.id == Child.id)
                        .LastOrDefault()
                            ? Child.full_category_family
                                .AsEnumerable()
                                .Reverse()
                                .Skip(1)
                                .Take(1)
                                .FirstOrDefault()
                                .name + "/" + Child.name
                            : Child.name
                };

                nCat.matched_words
                    .Add(Detail);

                Categories
                    .Add(nCat);
            }
            else
            {
                var cat = Categories
                    .FirstOrDefault(x => x.id == Child.id);

                if (!cat.matched_words.Contains(Detail))
                    cat.matched_words
                        .Add(Detail);
            }
        }

        public void GetFullModelNames(IEnumerable<ISubModel> Model, ref List<object> FullModels, ISubModel Parent = null)
        {
            foreach (var child in Model)
            {
                if (child.HasChildren)
                    GetFullModelNames(child.children, ref FullModels, child);
                else
                    FullModels.Add(new
                    {
                        id = child.id,
                        name = Parent == null
                            ? child.name
                            : $"{Parent.name}/{child.name}"
                    });
            }
        }

        public IEnumerable<ICategory> AdvancedBinarySearch(IModel Model, string Content, HashSet<string> TokenizedContent, char Delimiter)
        {
            /*
             * advanced binary search based on the model details and the content that is being searched
             * have the ordered details 
             * have the tokenized content
             * do binary search on details that don't have spaces in them
             * do regular .Contains on details that have spaces in them
             */
            var categories = new List<ICategory>();

            /*
             * iterate through the TokenizedContent and binary search the details that don't have spaces on each tokenized entry
             * iterate through the Details that have spaces and run a simple .Contains on each entry against the whole Content
             */
            var (singleWordsDetails, multipleWordsDetails) = this.GetAllDetailsTuple(Model, Delimiter);
            var singleWordsDetailsArray = singleWordsDetails.ToArray();

            Log?.LogDebug("count single words details={singleWordsDetailscount}", singleWordsDetails.Count());
            Log?.LogDebug("count multiple words details={multipleWordsDetailscount}", multipleWordsDetails.Count());

            foreach (var t in TokenizedContent)
            {
                if (Array.BinarySearch(singleWordsDetailsArray, t, StringComparer.InvariantCultureIgnoreCase) >= 0)
                    UpdateCategories(this.GetSubModelByDetail(Model, t, Delimiter), t, ref categories);
            }

            foreach (var mW in multipleWordsDetails)
            {
                if (Content.Contains(mW, StringComparison.InvariantCultureIgnoreCase))
                    UpdateCategories(this.GetSubModelByDetail(Model, mW, Delimiter), mW, ref categories);
            }

            Log?.LogDebug("count categories={categoriesCount}", categories.Count());

            return categories
                .AsEnumerable()
                .OrderBy(x => x.name);
        }

        public IEnumerable<string> GetAllDetails(IModel Model, char Delimiter)
        {
            var allDetails = new List<string>();
            var stack = new Stack<SubModel>(Model.model);

            while (stack.Any())
            {
                var next = stack.Pop();

                allDetails
                    .AddRange(this.SaveSplit(next.details, Delimiter));

                foreach (var child in next.children)
                    stack.Push(child);
            }

            return allDetails
                .AsEnumerable()
                .OrderBy(x => x);
        }
        public (IEnumerable<string> SingleWords, IEnumerable<string> MultipleWords) GetAllDetailsTuple(IModel Model, char Delimiter)
        {
            return MemoryCache.GetOrCreate($"{Model.id}-words", entry =>
            {
                var singleWordsDetails = new List<string>();
                var multipleWordsDetails = new List<string>();
                var stack = new Stack<SubModel>(Model.model);

                while (stack.Any())
                {
                    var next = stack.Pop();

                    foreach (var d in this.SaveSplit(next.details, Delimiter))
                    {
                        if (!d.Contains(' '))
                            singleWordsDetails.Add(d);
                        else
                            multipleWordsDetails.Add(d);
                    }

                    foreach (var child in next.children)
                        stack.Push(child);
                }

                return (singleWordsDetails.OrderBy(x => x),
                    multipleWordsDetails.OrderBy(x => x));
            });
        }

        public ISubModel GetSubModelByDetail(IModel Model, string Detail, char DetailDelimiter)
        {
            var stack = new Stack<SubModel>(Model.model);

            while (stack.Any())
            {
                var next = stack.Pop();

                if (!string.IsNullOrWhiteSpace(next.details))
                {
                    if (Array.BinarySearch(next.details.SaveSplitAsArray(DetailDelimiter),
                        Detail,
                        StringComparer.InvariantCultureIgnoreCase) >= 0)
                        return MemoryCache.GetOrCreate($"{Model.id}-{next.id}", entry =>
                        {
                            return next;
                        });
                }

                foreach (var child in next.children)
                    stack.Push(child);
            }

            return null;
        }

        public HashSet<string> SaveSplit(string Details, char Delimiter)
        {
            if (string.IsNullOrWhiteSpace(Details)) return new HashSet<string>();

            return new HashSet<string>(Details
                .Split(Delimiter,
                    StringSplitOptions.RemoveEmptyEntries));
        }

        public HashSet<string> Tokenize(string Content, char[] TokenizerDelimiters, string[] ScrubWords)
        {
            if (string.IsNullOrWhiteSpace(Content)) return new HashSet<string>();

            /*
             * when the content is tokenized, we should iterate through it and remove unnessecary words that mean nothing to the model
             * such as "I", "am", "not", etc. simply common words
             */
            var splitContent = Content
                .Split(TokenizerDelimiters, StringSplitOptions.RemoveEmptyEntries);

            var tokenizedContent = new HashSet<string>(ScrubWords != null && ScrubWords.Count() > 0
                ? splitContent.Except(ScrubWords, StringComparer.InvariantCultureIgnoreCase)
                : splitContent);

            return tokenizedContent;
        }
    }
}
