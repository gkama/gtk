﻿using System;
using System.Collections.Generic;
using System.Text;

using gkama.nlp.finance.data;

namespace gkama.nlp.finance.services
{
    public interface INLProcessingRepository
    {
        IEnumerable<IModel> GetTenant(string TenantId);
        IModel GetModel(string ModelId);
        IModel SortModel(string ModelId);
        IModelSettings GetModelSettings(string ModelId);
        IEnumerable<string> GetCategories(string ModelId);
        IEnumerable<ICategory> Categorize(string ModelId, string Content);
        IEnumerable<ICategory> AdvancedBinarySearch(IModel Model, string Content, HashSet<string> TokenizedContent, char Delimiter);
        (IEnumerable<string> SingleWords, IEnumerable<string> MultipleWords) GetAllDetailsTuple(IModel Model, char Delimiter);
        ISubModel GetSubModelByDetail(IModel Model, string Detail, char DetailDelimiter);
    }
}
