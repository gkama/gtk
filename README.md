# personal projects
this is a repository for open source projects by [georgi kamacharov](https://gkamacharov.com)

### gkama.graph.ql
sample project for `GraphQL` for `.NET Core`

- ASP.NET Core 2.2 API
- GraphQL with ui/playground
- Entity Framework Core for creating a in-memory database with sample datasets
- Defined query and schema for countries with neighbouring countries

### gkama.nlp.finance
`Natural Language Processing (NLP)` analyzer for financial words and phrases

- ASP.NET Core 2.2 API
- REST
- MongoDB